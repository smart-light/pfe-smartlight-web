import httplib
import hashlib
import json
import urllib

class SmartLight:

    instance = None

    def __init__(self, user, passwd, host='127.0.0.1', port=8000):

        self.user = user
        self.passwd = passwd
        self.host = host
        self.port = port
        self.token = ''
        SmartLight.instance = self

    @classmethod
    def getInstance(cls):
        return cls.instance

    def account(self):
        return self._callApi('/account')

    def addAccount(self, user, admin=False):
        arg = '/' + user

        if admin:
           arg += '/admin'

        return self._callApi('/account/add' + arg)

    def call(self, groupLamp, args):
        self._callApi('/module/%s/call' % groupLamp, args)

    def getSavedData(self, groupLamp):
        return self._callApi('/module/%s/config' % groupLamp)

    def groupLampList(self):
        return self._callApi('/group-lamp')

    def info(self):
        return self._callApi('/info')

    def login(self):
        salt = self._callApi('/login/' + self.user)#['salt']
        hashLogin = hashlib.sha256(salt + self.passwd).hexdigest()

        self.token = self._callApi('/login/' + self.user + '/' + hashLogin)#['token']

    def loadModule(self, groupName, moduleName):
        return self._callApi('/group-lamp/%s/module/%s' % (groupName, moduleName))

    def logout(self):
        return self._callApi('/logout')

    def modulesList(self):
        return self._callApi('/module')
        
    def _callApi(self, url, params={}, type='GET', isReplayed=False):

        header = {}
        
        if len(params) > 0:
            type='POST'
            
        if self.token:
            header['X-SmartLight-Token'] = self.token

        request = httplib.HTTPConnection(self.host, self.port)
        request.request(type, url, urllib.urlencode(params), header)

        result = request.getresponse()
        data = result.read()

        try:
            data = json.loads( data )
        except Exception as e:
            pass

        if not isReplayed and isinstance(data, dict) and 'msg' in data and data['msg'] == "login required":
            self.login()
            data = self._callApi(url, type, isReplayed=True)

        request.close()

        return data
