#!/usr/bin/python
# -*- coding: utf-8 -*-
from libs.api import SmartLight

import bottle
import os


def loadModules():
    path = '%s/modules/' % os.path.dirname(os.path.abspath(__file__))
    for moduleName in os.listdir(path):
        if os.path.isdir('%s%s' % (path, moduleName)):
            for name in os.listdir('%s%s/pages/' % (path, moduleName)):

                if '.pyc' in name or '__init__' in name:
                    continue

                if '.py' in name:
                    name = name.replace('.py', '')

                __import__('modules.%s.pages.%s' % (moduleName, name), globals(), locals())

            if os.path.isdir('%s%s/views' % (path, moduleName)):
                bottle.TEMPLATE_PATH[:0] = [ '%s%s/views' % (path, moduleName) ]


def start(port=80):
    loadModules()

    try:
        SmartLight('admin', '324c0f8e73cb16614fed0add7792ac958d5d3162428ca21b359015f48dbb522e', '127.0.0.1').login()
    except Exception as e:
        print "Core injoignable\n %s" % e

    bottle.debug()
    bottle.run(host='0.0.0.0', port=port, reloader=True)


if __name__ == "__main__":
    start(8001)
