from libs.api import SmartLight
from libs.bottlesession import *

from bottle import *
import sys

session_manager = PickleSession()
valid_user = authenticator(session_manager, login_url='/login')


@get('/modules/<groupeLamp>/<moduleName>')
@post('/modules/<groupeLamp>/<moduleName>')
@view('groupLamp.tpl')
@valid_user()
def module(groupeLamp, moduleName):

    smartLight = SmartLight.getInstance()
    smartLight.loadModule(groupeLamp, moduleName)

    try:
        exec 'moduleData=sys.modules[\'modules.%s.pages.module\'].home(\'%s\')' % (moduleName, groupeLamp)

    except Exception as e:
        # print e
        moduleData = {}

    return {
        'groupeLamp': groupeLamp,
        'moduleName': moduleName,
        'moduleData': moduleData,
	    'moduleList': smartLight.modulesList(),
    }
