from libs.api import SmartLight

from bottle import *
from libs.bottlesession import *

session_manager = PickleSession()
valid_user = authenticator(session_manager, login_url='/login')

@get('/')
@view('home.tpl')
@valid_user()
def index():
    return {
        'groupLamp': SmartLight.getInstance().groupLampList(),
    }

@error(404)
@view('base.tpl')
def error404(error):
    return {'content': 'Page introuvable'}

@error(500)
@view('base.tpl')
def error500(error):
    return {'content': 'Erreur SmartLight Core est demarer ?'}
