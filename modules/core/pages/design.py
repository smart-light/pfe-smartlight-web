from bottle import *
import os


PATH_ROOT_STATIC = os.path.dirname(os.path.abspath(__file__)) + '/../static/'
PATH_STATIC = []
PATH_STATIC_INIT = False


@route('/static/<filename:path>')
def staticFiles(filename):
    result = static_file(filename, root=PATH_ROOT_STATIC)

    if result and result.status_code == 200:
        return result

    if not PATH_STATIC_INIT:
        initStaticPath()

    for path in PATH_STATIC:
        result = static_file(filename, root=path)

        if result and result.status_code == 200:
            return result

    return result

@route('/theme')
@route('/theme/:theme')
@view('theme')
def theme(theme = ''):
    themeList = []

    for repertoire in os.listdir( PATH_ROOT_STATIC + '/theme' ):

        if os.path.isdir( PATH_ROOT_STATIC + '/theme/' + repertoire ):
            themeList.append( repertoire )

    if theme in themeList:
        response.set_cookie('theme', theme, path = '/', expires = time.time() + 9999999 )
        redirect('/theme')

    themeList.sort()
    return {'themeList': themeList}


def initStaticPath():
    global PATH_STATIC
    global PATH_STATIC_INIT

    PATH_STATIC_INIT = True
    path = '%s/../../' % os.path.dirname(os.path.abspath(__file__))
    for name in os.listdir(path):
        if name != 'core' and os.path.isdir('%s/%s/static/' % (path, name)):
            PATH_STATIC.append('%s/%s/static/' % (path, name))
