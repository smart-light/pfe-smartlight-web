__author__ = 'y0no'

from bottle import *
from libs.api import SmartLight
from libs.bottlesession import *

session_manager = PickleSession()
valid_user = authenticator(session_manager, login_url='/login')


@get('/login')
@post('/login')
@view('auth/login.tpl')
def login():
    users = {'guest': 'guest' }

    result = {
        'formLogin': [
            {
                'name': 'username',
                'label': 'Utilisateur',
            },
            {
                'name': 'password',
                'label': 'Mot de passe',
                'type': 'password',
            },
        ],
    }

    username = request.forms.get('username')
    password = request.forms.get('password')

    if not username or not password:
        if len(request.POST) > 0:
            result['error'] = 'Please specify username and password'
        return result

    session = session_manager.get_session()
    session['valid'] = False

    if users.get(username) == password:
        session['valid'] = True
        session['name'] = username

    session_manager.save(session)

    if not session['valid']:
        result['error'] = 'Username or password is invalid'
        return result

    redirect(request.COOKIES.get('validuserloginredirect', '/'))


@get('/logout')
def logout():
   session = session_manager.get_session()
   session['valid'] = False
   session_manager.save(session)
   bottle.redirect('/login')
