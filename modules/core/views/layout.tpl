% from bottle import request
% theme = request.get_cookie('theme')
% if not theme:
%     theme = 'slate'
% end
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>{{get('title', 'SmartLight')}}</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/static/css/theme.css" rel="stylesheet">
		<link href="/static/theme/{{theme}}/style.css" rel="stylesheet">

		<!--[if lt IE 9]>
			<script src="/static/js/html5shiv.js"></script>
			<script src="/static/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>
		<script src="/static/js/jquery.min.js"></script>
		% include navbar

		<div class="section-preview">
			<div class="container">
				% include
			</div>
		</div>

		<script src="/static/js/bootstrap.min.js"></script>
	</body>
</html>
