% if defined('form'):
	<form class="form-horizontal" role="form" method="post">

		% for input in form:
			% input['label'] = input['label'] if input.has_key('label') else ''
			% input['name'] = input['name'] if input.has_key('name') else ''
			% input['type'] = input['type'] if input.has_key('type') else 'text'

			% if 'checkbox' == input['type']:

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="{{ input['name'] }}"> {{ input['label'] }}
							</label>
						</div>
					</div>
				</div>
			% elif 'select' == input['type']:

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">{{ input['label'] }}</label>
					<div class="col-sm-10">
						<select class="form-control" name="{{ input['name'] }}">
                            % selected = input['value'] if input.has_key('value') else None
							% for v in input['choices']:
                                <option value="{{v}}" 
                                    % if selected == v:
                                        selected="selected"
                                    % end
                                >{{input['choices'][v]}}</option>
                            % end
						</select>
					</div>
				</div>
			% else:
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">{{ input['label'] }}</label>
					<div class="col-sm-10">
						<input type="{{ input['type'] }}" class="form-control" name="{{ input['name'] }}" placeholder="{{ input['label'] }}" 
                            % if input.has_key('value'):
                                value="{{input['value']}}"
                            % end
                        />
					</div>
				</div>
			%end
		% end

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">{{ get('form-submit', 'Sauvegarder') }}</button>
			</div>
		</div>
	</form>
% end
