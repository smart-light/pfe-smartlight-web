<div class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a href="/" class="navbar-brand">SmartLight</a>
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>

		<div class="navbar-collapse collapse" id="navbar-main">
			<ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="admin">Administration <span class="caret"></span></a>
					<ul class="dropdown-menu" aria-labelledby="admin">
						<li><a tabindex="-1" href="/groupLamp">Groupes de lampes</a></li>
					</ul>
				</li>
	
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="session">Session <span class="caret"></span></a>
					<ul class="dropdown-menu" aria-labelledby="session">
						<li><a tabindex="-1" href="/theme">Theme</a></li>
						<li class="divider"></li>
						<li><a tabindex="-1" href="/logout">Déconnexion</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
