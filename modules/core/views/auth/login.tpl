% rebase layout **locals()

%if defined('error'):
    <div class="alert alert-danger"><b>Notice</b> {{error}}</div>
%end

% include form form=formLogin
