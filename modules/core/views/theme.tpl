% rebase layout **locals()

<div class="row">
	%for themeName in themeList:
		<div class="col-lg-4 col-sm-6">
			<div class="bsa well">
				<div class="image">
					<img src="/static/theme/{{themeName}}/thumbnail.png" class="img-responsive" alt="{{themeName}}">
				</div>

				<div class="options" style="text-align:center; margin-top:10px">
					<div class="btn-group"><a class="btn btn-info" href="/theme/{{themeName}}">Utiliser</a></div>
				</div>
			</div>
		</div>
	%end
</div>