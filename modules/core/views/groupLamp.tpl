% rebase layout **locals()

<div class="row">
    <div class="col-md-4">
        <div class="well sidebar-nav">
            <ul class="nav nav-list">
		        <li class="nav-header">Modules Disponibles</li>

                % for module in moduleList:
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="/modules/{{ groupeLamp }}/{{ module }}">{{ module }}</a></li>
                % end
            </ul>
        </div>
    </div>

    % if moduleData:
        <div class="col-md-8 ">
            <div class="well">
                % if 'form' in moduleData:
                    % include form form=moduleData['form']
                % else:
                    % include {{view}} data=moduleData
                % end
	        </div>
        </div>
    % end
</div>
