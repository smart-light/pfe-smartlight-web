% rebase layout **locals()

<div class="row">
	%for name in groupLamp:
		<div class="col-lg-4 col-sm-6">
			<div class="bsa well" style="text-align:center">
				<a data-toggle="lightbox" href="/modules/{{ name }}/{{ groupLamp[name]['moduleName'] }}">
					<b>{{ name }}</b>
				</a>
			</div>
		</div>
	%end
</div>
