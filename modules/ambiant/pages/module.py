# -*- coding: utf-8 -*-

from libs.api import SmartLight
from bottle import *

from collections import OrderedDict
from libs.bottlesession import *

def home(groupeLamp):

    # changement de l'ambiance :
    loadAmbiant = request.forms.get('ambiant')
    loadColor = request.forms.get('color')

    data = {}
    if loadAmbiant:
        data['ambiant'] = loadAmbiant

    if loadColor:
        data['color'] = loadColor

    if data:
        time = request.forms.get('time')
        if time:
            data['time'] = time

        SmartLight.getInstance().call(groupeLamp, data)


    ambiantList = SmartLight.getInstance().getSavedData(groupeLamp)
    value = None

    for ambiant in ambiantList:
        if 'selected' in ambiantList[ambiant] and ambiantList[ambiant]['selected']:
            value = ambiant
        ambiantList[ambiant] = ambiant

    return {
	    'form': [
            {
                'name': 'ambiant',
                'label': 'Ambiance actuelle',
                'type': 'select',
                'choices': OrderedDict(sorted(ambiantList.items(), key=lambda t: t[1])),
                'value': value,
            },
            {
                'name': 'time',
                'label': 'Temps de transition',
                'type': 'number',
                'value': 2,
            },
        ],
    }
